AWSTemplateFormatVersion: "2010-09-09"
Description: Egress Lambda Example

Parameters:
  EndpointUrl:
    Description: "The URL of the public endpoint that the Lambda Function needs to make requests to"
    Type: "String"
    Default: "https://imdb8.p.rapidapi.com/auto-complete"

  EndpointIpAddressRange:
    Description: "CIDR range of the public endpoint the Lambda function needs to make requests to (should include Subnet mask, e.g. 1.2.3.4/32)"
    Type: "String"
    Default: "52.45.180.163/32"

Resources:

##### NETWORKING RESOURCES #####

  VPC:
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock: 10.0.0.0/20
      EnableDnsHostnames: True
      EnableDnsSupport: True
      InstanceTenancy: default
      Tags:
        - Key: Name 
          Value: ServerlessVPC

  PublicSubnet1:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone: "us-east-1a"
      CidrBlock: 10.0.0.0/22
      MapPublicIpOnLaunch: True
      VpcId: !Ref VPC
      Tags:
        - Key: Name 
          Value: ServerlessVPC

  PrivateSubnet1:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone: "us-east-1a"
      CidrBlock: 10.0.8.0/22
      MapPublicIpOnLaunch: False
      VpcId: !Ref VPC
      Tags:
        - Key: Name 
          Value: ServerlessVPC

  PrivateSubnet2:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone: "us-east-1b"
      CidrBlock: 10.0.12.0/22
      MapPublicIpOnLaunch: False
      VpcId: !Ref VPC
      Tags:
        - Key: Name 
          Value: ServerlessVPC

  NatGatewayElasticIp:
    Type: AWS::EC2::EIP
    Properties: 
      Domain: vpc

  InternetGateway:
    Type: AWS::EC2::InternetGateway

  InternetGatewayAttachment:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties: 
      InternetGatewayId: !Ref InternetGateway
      VpcId: !Ref VPC

  NatGateway:
    Type: AWS::EC2::NatGateway
    Properties: 
      AllocationId: !GetAtt NatGatewayElasticIp.AllocationId
      ConnectivityType: public
      SubnetId: !Ref PublicSubnet1

  RouteTableForPrivateSubnets:
    Type: AWS::EC2::RouteTable
    Properties: 
      VpcId: !Ref VPC

  NatGatewayRoute:
    Type: AWS::EC2::Route
    Properties:
      DestinationCidrBlock: '0.0.0.0/0'
      NatGatewayId: !Ref NatGateway
      RouteTableId: !Ref RouteTableForPrivateSubnets

  Private1ToNatRouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties: 
      RouteTableId: !Ref RouteTableForPrivateSubnets
      SubnetId: !Ref PrivateSubnet1

  Private2ToNatRouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties: 
      RouteTableId: !Ref RouteTableForPrivateSubnets
      SubnetId: !Ref PrivateSubnet2

  RouteTableForPublicSubnets:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC

  InternetGatewayRoute:
    Type: AWS::EC2::Route
    Properties:
      DestinationCidrBlock: '0.0.0.0/0'
      GatewayId: !Ref InternetGateway
      RouteTableId: !Ref RouteTableForPublicSubnets

  Public1ToInternetGatewayRouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties: 
      RouteTableId: !Ref RouteTableForPublicSubnets
      SubnetId: !Ref PublicSubnet1

  LambdaFunctionSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupName: LambdaFunctionSecurityGroup
      GroupDescription: "A security group to define network ingress and egress access for the networkedlambdafunction resource."
      SecurityGroupEgress:
        - IpProtocol: tcp
          CidrIp: !Ref EndpointIpAddressRange
          Description: ip of external api
          FromPort: 443
          ToPort: 443
      SecurityGroupIngress:
        - IpProtocol: tcp
          CidrIp: 0.0.0.0/0
          Description: insert desc
          FromPort: 0
          ToPort: 65535
      VpcId: !Ref VPC

##### IAM #####

  LambdaFunctionRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow 
            Principal:
              Service: lambda.amazonaws.com 
            Action:
              - sts:AssumeRole
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole

  LambdaFunctionRolePolicy:
    Type: AWS::IAM::Policy
    Properties:
      PolicyDocument: 
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Action:
              - 'ec2:CreateNetworkInterface'
              - 'ec2:DescribeNetworkInterfaces'
              - 'ec2:DeleteNetworkInterface'
            Resource: '*'
      PolicyName: LambdaFunctionRolePolicy
      Roles: 
        - !Ref LambdaFunctionRole
      
  LambdaInvokePermission:
    Type: AWS::Lambda::Permission
    Properties:
      Action: lambda:InvokeFunction
      FunctionName: !GetAtt LambdaFunction.Arn
      Principal: apigateway.amazonaws.com

##### SERVERLESS COMPONENTS #####

  LambdaFunction:
    Type: AWS::Lambda::Function
    Properties:
      FunctionName: EgressLambdaFunction
      Role: !GetAtt LambdaFunctionRole.Arn
      Handler: index.lambda_handler
      Environment:
        Variables:
          EndpointUrl: !Ref EndpointUrl
      Runtime: python3.6
      VpcConfig:
        SubnetIds:
         - !Ref PrivateSubnet1
         - !Ref PrivateSubnet2
        SecurityGroupIds:
          - !Ref LambdaFunctionSecurityGroup
      Code: 
        ZipFile: |
          import os
          def lambda_handler(event, context):
            print('hello from egress lambda function')
            from botocore.vendored import requests

            url = os.environ['EndpointUrl']

            querystring = {"q":"game of thr"}

            headers = {'x-rapidapi-host': 'imdb8.p.rapidapi.com'}

            response = requests.request("GET", url, headers=headers, params=querystring)

            print(response.text)
            return {
              "statusCode": 200,
              "headers": {
                  "Content-Type": "application/json"
              }
            }


