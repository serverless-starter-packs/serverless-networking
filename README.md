# Serverless Networking Examples

This repository consists two CloudFormation templates that show how private networking can be configured for AWS Lambda and API Gateway, in order to improve the security posture of AWS serverless applications.

The first, [Basic Networking](basic_networking.yml) shows an private API Gateway with a single endpoint to a Lambda function that is hosted in a customer-defined VPC.  This architecture is shown below.

![image](BasicNetworking.png)

The second, [Egress Lambda](internet_access.png) shows how a Lambda function, hosted inside a VPC, can be given strictly controlled egress access out to the public internet.  Again the architecture is shown below.

![image](EgressLambda.png)